package siwei_sdk

import (
	"encoding/json"
	"io/ioutil"
	"net/url"
)

//se rs(道路搜索服务)入参
type SeRsReq struct {
	// 道路名称
	Name string
	// 所在城市名称
	City *string
}

//se rs(道路搜索服务)返回结果
type SeRsResult struct {
	Result struct {
		DistrictText string `json:"district_text"`
		Address      string `json:"address"`
		Road         struct {
			LimitSpeed    string `json:"limit_speed"`
			Linkid        string `json:"linkid"`
			Lng           string `json:"lng"`
			Distance      int    `json:"distance"`
			Urban         string `json:"urban"`
			Name          string `json:"name"`
			Width         string `json:"width"`
			RoadLevel     string `json:"road_level"`
			Lanenumber    string `json:"lanenumber"`
			Roadtype      string `json:"roadtype"`
			Lat           string `json:"lat"`
			RoadLevelCode int    `json:"road_level_code"`
		} `json:"road"`
		District string `json:"district"`
		Point    struct {
			Number string  `json:"number"`
			Lng    float64 `json:"lng"`
			Name   string  `json:"name"`
			Lat    float64 `json:"lat"`
		} `json:"point"`
		RoadAddress string `json:"road_address"`
	} `json:"result"`
	Status string `json:"status"`
}

// 逆地址编码(货车)
func (s *Sdk) SeRs(req *SeRsReq) (*SeRsResult, error) {
	// 参数处理
	v := url.Values{}
	v.Add("output", "json")
	v.Add("name", req.Name)
	if req.City != nil {
		v.Add("city", *req.City)
	}

	// 请求接口
	res, err := s.DoRequest("SE_RS", &v)
	if err != nil {
		return nil, err
	}
	if res.StatusCode >= 200 && res.StatusCode < 300 {
		bodyBs, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		result := SeRsResult{}
		err = json.Unmarshal(bodyBs, &result)
		if err != nil {
			return nil, err
		}
		return &result, nil
	}
	return nil, nil
}
