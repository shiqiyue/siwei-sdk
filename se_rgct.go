package siwei_sdk

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
)

//rgct入参
type RgctReq struct {
	// 点坐标
	Point Point
	// 坐标类型 取值gcj02、wgs84
	Cd *string

	// 查询类型
	Type *RgcType
}

type RgctResult struct {
	Result struct {
		//行政区划
		DistrictText string `json:"district_text"`
		//地址
		Address string `json:"address"`
		Road    struct {
			//道路限速	有货车限速值时是货车限速值；货车无限速值时为小车限速值
			LimitSpeed string `json:"limit_speed"`
			//道路id号
			Linkid string `json:"linkid"`
			//经度
			Lng string `json:"lng"`
			//查询点距离最近的道路的距离	,单位“米”
			Distance int `json:"distance"`
			//是否城市道路	0：不是城市道路；1：是城市道路
			Urban string `json:"urban"`
			//道路名称
			Name string `json:"name"`
			//道路宽度
			Width string `json:"width"`
			//道路等级名称
			RoadLevel string `json:"road_level"`
			//最近道路车道的数量
			Lanenumber string `json:"lanenumber"`
			//道路的类型
			Roadtype string `json:"roadtype"`
			//纬度
			Lat string `json:"lat"`
			//道路等级编码
			RoadLevelCode int `json:"road_level_code"`
		} `json:"road"`
		//行政编码
		District string `json:"district"`
		Point    struct {
			//门牌号
			Number string `json:"number"`
			//经度
			Lng float64 `json:"lng"`
			//查询点 最近的POI 信息的名称
			Name string `json:"name"`
			//纬度
			Lat float64 `json:"lat"`
		} `json:"point"`
		RoadAddress string `json:"road_address"`
	} `json:"result"`
	//Ok：代表成功；error：代表失败
	Status string `json:"status"`
}

// 逆地址编码(货车)
func (s *Sdk) Rgct(req *RgctReq) (*RgctResult, error) {
	// 参数处理
	v := url.Values{}
	v.Add("st", "Rgc3")
	v.Add("output", "json")
	v.Add("point", req.Point.String())
	if req.Type != nil {
		v.Add("type", fmt.Sprintf("%d", *req.Type))
	}

	// 请求接口
	res, err := s.DoRequest("SE_RGC3", &v)
	if err != nil {
		return nil, err
	}
	if res.StatusCode >= 200 && res.StatusCode < 300 {
		bodyBs, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		result := RgctResult{}
		fmt.Println(string(bodyBs))
		err = json.Unmarshal(bodyBs, &result)
		if err != nil {
			return nil, err
		}
		return &result, nil
	}
	return nil, nil
}
