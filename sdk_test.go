package siwei_sdk

import (
	"fmt"
	"testing"
)

func newSdk() *Sdk {
	sdk := NewSdk("qzzxwx")
	return sdk
}

func TestRgc3(t *testing.T) {
	sdk := newSdk()
	rgb3Req := Rgc3Req{
		Point: Point{X: 118.609111, Y: 24.912725},
	}
	cd := "wgs84"
	rgb3Req.Cd = &cd
	result, err := sdk.Rgc3(&rgb3Req)
	fmt.Println(result, err)
}
func TestRgct(t *testing.T) {
	sdk := newSdk()
	rgb3Req := RgctReq{
		Point: Point{X: 118.65832, Y: 24.86953},
	}
	cd := "wgs84"
	rgb3Req.Cd = &cd
	rgb3Req.Type = &RGC_TYPE_ROAD
	result, err := sdk.Rgct(&rgb3Req)
	fmt.Println(result, err)
}

func TestSeRs(t *testing.T) {
	sdk := newSdk()
	var city = "泉州市"
	req := SeRsReq{
		Name: "东海大街",
		City: &city,
	}
	rs, err := sdk.SeRs(&req)
	fmt.Println(*rs, err)
}
