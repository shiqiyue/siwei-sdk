package siwei_sdk

import (
	"net/http"
	"net/url"
)

var (
	// 请求基础链接
	BASEURL = "http://a.qqearth.com:81"
	// 默认坐标系
	DEFAULT_CD = "wgs84"
)

type Sdk struct {
	Uid    string
	Client *http.Client
}

// 新建sdk
func NewSdk(uid string) *Sdk {
	client := http.DefaultClient
	return &Sdk{Uid: uid, Client: client}
}

func (s *Sdk) DoRequest(path string, values *url.Values) (*http.Response, error) {
	values.Add("uid", s.Uid)
	u := url.URL{
		Scheme:     "http",
		Host:       "a.qqearth.com:81",
		Path:       path,
		ForceQuery: false,
		RawQuery:   values.Encode(),
	}
	requestUrl := u.String()
	response, err := s.Client.Get(requestUrl)
	return response, err
}
