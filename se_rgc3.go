package siwei_sdk

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
)

type RgcType int

var (
	// 地标查询
	RGC_TYPE_LANDMARK RgcType = 1
	// 道路查询
	RGC_TYPE_ROAD RgcType = 10
	// 地标和道路查询
	RGC_TYPE_LANDMARK_AND_ROAD RgcType = 11
)

//rgc3入参
type Rgc3Req struct {
	// 点坐标
	Point Point
	// 坐标类型 取值gcj02、wgs84
	Cd *string
	// 偏移方向
	Angle *int
	// 输出poi类型，默认：最近的poi；adr=address有具体道路值的最近的poi；adr=hamlet:最近的村庄
	Adr *string
	// 查询类型
	Type *RgcType
	// 道路等级
	Roadlevel *int
}

type Rgc3Result struct {
	Result struct {
		DistrictText string `json:"district_text"`
		Address      string `json:"address"`
		Road         struct {
			LimitSpeed    string `json:"limit_speed"`
			Linkid        string `json:"linkid"`
			Lng           string `json:"lng"`
			Distance      int    `json:"distance"`
			Urban         string `json:"urban"`
			Name          string `json:"name"`
			Width         string `json:"width"`
			RoadLevel     string `json:"road_level"`
			Lanenumber    string `json:"lanenumber"`
			Roadtype      string `json:"roadtype"`
			Lat           string `json:"lat"`
			RoadLevelCode int    `json:"road_level_code"`
		} `json:"road"`
		District string `json:"district"`
		Point    struct {
			Number string  `json:"number"`
			Lng    float64 `json:"lng"`
			Name   string  `json:"name"`
			Lat    float64 `json:"lat"`
		} `json:"point"`
		RoadAddress string `json:"road_address"`
	} `json:"result"`
	Status string `json:"status"`
}

// 逆地址编码3
func (s *Sdk) Rgc3(req *Rgc3Req) (*Rgc3Result, error) {
	// 参数处理
	v := url.Values{}
	v.Add("st", "Rgc3")
	v.Add("output", "json")
	v.Add("point", req.Point.String())
	if req.Type != nil {
		v.Add("type", fmt.Sprintf("%d", *req.Type))
	}
	if req.Adr != nil {
		v.Add("adr", *req.Adr)
	}
	if req.Roadlevel != nil {
		v.Add("roadlevel", fmt.Sprintf("%d", *req.Roadlevel))
	}
	// 请求接口
	res, err := s.DoRequest("SE_RGC3", &v)
	if err != nil {
		return nil, err
	}
	if res.StatusCode >= 200 && res.StatusCode < 300 {
		bodyBs, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		result := Rgc3Result{}
		err = json.Unmarshal(bodyBs, &result)
		if err != nil {
			return nil, err
		}
		return &result, nil
	}
	return nil, nil
}
